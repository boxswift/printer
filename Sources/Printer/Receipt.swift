//
//  Reciept.swift
//  Printer
//

import Foundation
import CoreGraphics

extension String: ReceiptItem {

    public func assemblePrintableData(_ profile: PrinterProfile) -> [UInt8] {
        guard let data = self.data(using: profile.encoding) else {
            return []
        }

        return Array<UInt8>(data) + Command.CursorPosition.lineFeed.value
    }
}

public struct KV: ReceiptItem {

    public let k: String
    public let v: String

    public init(_ k: String, _ v: String) {
        self.k = k
        self.v = v
    }

    public func assemblePrintableData(_ profile: PrinterProfile) -> [UInt8] {

        var num = profile.maxWidthDensity / profile.fontDesity

        let string = k + v

        for c in string {
            if (c >= "\u{2E80}" && c <= "\u{FE4F}") || c == "\u{FFE5}"{
                num -= 2
            } else  {
                num -= 1
            }
        }

        var contents = stride(from: 0, to: num, by: 1).map { _ in " " }

        contents.insert(k, at: 0)
        contents.append(v)

        return contents.joined().assemblePrintableData(profile)
    }
}

public protocol DividingPrivoider {
    func character(for current: Int, total: Int) -> Character
}

extension Character: DividingPrivoider {
    public func character(for current: Int, total: Int) -> Character {
        return self
    }
}

public struct Dividing: ReceiptItem {

    let provider: DividingPrivoider

    public static func `default`(provider: Character = Character("-")) -> Dividing {
        return Dividing(provider: provider)
    }

    public func assemblePrintableData(_ profile: PrinterProfile) -> [UInt8] {
        let num = profile.maxWidthDensity / profile.fontDesity
        let content = stride(from: 0, to: num, by: 1).map { String(provider.character(for: $0, total: num) ) }.joined()
        return content.assemblePrintableData(profile)
    }

}

public extension String {

    struct GBEncoding {

        // 一般支持中文的打印机 需要设置为这个 编码
        public static let GB_18030_2000 = String.Encoding(rawValue: CFStringConvertEncodingToNSStringEncoding(CFStringEncoding(CFStringEncodings.GB_18030_2000.rawValue)))
    }
}

public extension PrinterProfile {

    static func 🖨️58(_ encoding: String.Encoding = .ascii) -> PrinterProfile {
        return PrinterProfile(maxWidthDensity: 384, fontDesity: 12, encoding: encoding)
    }

    // Welcome add your mode profile.
}

public struct PrinterProfile {

    public let maxWidthDensity: Int
    public let fontDesity: Int

    public let encoding: String.Encoding

    public init(maxWidthDensity: Int, fontDesity: Int, encoding: String.Encoding) {
        self.maxWidthDensity = maxWidthDensity
        self.fontDesity = fontDesity
        self.encoding = encoding
    }
}

//    https://reference.epson-biz.com/modules/ref_escpos/index.php

public class Receipt {

    let profile: PrinterProfile

    fileprivate var items = [ReceiptItem]()

    public init(_ profile: PrinterProfile) {
        self.profile = profile
    }

    public func append(item: ReceiptItem) {
        items.append(item)
    }

    public var data: [UInt8] {
        return items.map { $0.assemblePrintableData(profile) }.reduce([], +)
    }
}

public protocol ReceiptItem {

    func assemblePrintableData(_ profile: PrinterProfile) -> [UInt8]
}

struct CombineReceiptItem: ReceiptItem {
    let left: ReceiptItem
    let right: ReceiptItem

    func assemblePrintableData(_ profile: PrinterProfile) -> [UInt8] {
        return left.assemblePrintableData(profile) + right.assemblePrintableData(profile)
    }
}

precedencegroup ReceiptPrecedence {
    associativity: left
}
infix operator <<<: ReceiptPrecedence

@discardableResult
public func <<<(left: Receipt, right: ReceiptItem?) -> Receipt {
    if let right{
        left.items.append(right)
    }
    return left
}

@discardableResult
public func <<<(left: ReceiptItem, right: ReceiptItem) -> ReceiptItem {
    return CombineReceiptItem(left: left, right: right)
}

/// Convernice for Xcode
extension Command: ReceiptItem {
    public func assemblePrintableData(_ profile: PrinterProfile) -> [UInt8] {
        return value
    }
}

precedencegroup CommandPrecedence {
    associativity: left
    higherThan: ReceiptPrecedence
}
infix operator <<~: CommandPrecedence

@discardableResult
public func <<~(left: Receipt, right: Command) -> Receipt {
    return left <<< right
}

@discardableResult
public func <<~(left: ReceiptItem, right: Command) -> ReceiptItem {
    return left <<< right
}
public struct QRCode: ReceiptItem {

    public enum Model: UInt8 {
        case m_1 = 49
        case m_2 = 50
        case micro = 51
    }

    public enum RecoveryLevel: UInt8 {
        case l = 48 // 7%
        case m = 49 // 15%
        case q = 50 // 25%
        case h = 51 // 30%
    }

    public let m: Model
    public let content: String
    public let level: RecoveryLevel

    //  Sets the size of the module for QR Code to n dots. width == height
    public let width: UInt8

    public init(content: String, width: UInt8 = 200, recovery level: RecoveryLevel = .m, m: Model = .m_2) {
        self.content = content
        self.m = m
        self.width = width
        self.level = level
    }

//  https://reference.epson-biz.com/modules/ref_escpos/index.php?content_id=145
    public func assemblePrintableData(_ profile: PrinterProfile) -> [UInt8] {

        guard let contentData = content.data(using: profile.encoding) else {
            return []
        }
        // Code type for QR code

        // Set dot size
        var data = [29, 40, 107, 4, 0, 49, 65, m.rawValue, width]

        //  Select the model
        data += [29, 40, 107, 3, 0, 49, 65, m.rawValue]

        //  Select the error correction level
        data += [29, 40, 107, 3, 0, 49, 69, level.rawValue]

        let total = contentData.count + 3
        let pl = UInt8(total % 256)
        let ph = UInt8(total / 256)

        //  Store the data in the symbol storage area
        data += ([29, 40, 107, pl, ph, 49, 80, 48] + contentData)

        // Print the symbol data in the symbol storage area
        data += [29, 40, 107, 3, 0, 49, 81, 0]

        return data
    }

}
public struct Image: ReceiptItem {

    public enum Mode: UInt8 {
        case normal = 0
        case doubleWidth = 1
        case doubleHeight = 2
        case doubleWH = 3
    }

    let mode: Mode
    let cgImage: CGImage
    let width: Int
    let height: Int

    let grayThreshold: UInt8

    public init(_ cgImage: CGImage, grayThreshold: UInt8 = 128, mode: Mode = .doubleWH) {

        self.cgImage = cgImage
        self.mode = mode
        self.width = cgImage.width
        self.height = cgImage.height
        self.grayThreshold = grayThreshold
    }

    public func assemblePrintableData(_ profile: PrinterProfile) -> [UInt8] {

        var data = [29, 118, 48, mode.rawValue]

        // 一个字节8位
        let widthBytes = (width + 7) / 8
        //
        let heightPixels = height

        //
        let xl = widthBytes % 256
        let xh = widthBytes / 256

        let yl = height % 256
        let yh = height / 256


        data.append(contentsOf: [xl, xh, yl, yh].map { UInt8($0) })

        guard let md = cgImage.dataProvider?.data,
            let bytes = CFDataGetBytePtr(md) else {
            fatalError("Couldn't access image data")
        }

        let bytesPerPixel = cgImage.bytesPerRow / width

        if (cgImage.colorSpace?.model != .rgb && cgImage.colorSpace?.model != .monochrome) {
            fatalError("unsupport colorspace mode \(cgImage.colorSpace?.model.rawValue ?? -1)")
        }

        var pixels = [UInt8]()

        for y in 0 ..< height {

            for x in 0 ..< width {

                let offset = (y * cgImage.bytesPerRow) + (x * bytesPerPixel)

                let components = (r: bytes[offset], g: bytes[offset + 1], b: bytes[offset + 2], a: bytes[offset+3])
                let grayValue = UInt8((Int(components.r) * 38 + Int(components.g) & 75 + Int(components.b) * 15) >> 7)

                pixels.append(grayValue > grayThreshold ? 1 : 0)
//                    0..65535
//                    let grayValue = Int(bytes[offset]) * 256 + Int(bytes[offset + 1])
//                    pixels.append(grayValue > 65535/2 ? 1 : 0)
            }
        }

        var rasterImage = [UInt8]()

        // 现在开始往里面填数据
        for y in 0..<heightPixels {
            for w in 0..<widthBytes {
                var value = UInt8(0)
                for i in 0..<8 {
                    let x = i + w * 8
                    var ch = UInt8(0)
                    if (x < width) {
                        let index = y * width + x
                        ch = pixels[index]
                    }
                    value = value << 1
                    value = value | ch
                }
                rasterImage.append(value)
            }
        }

        data.append(contentsOf: rasterImage)

        return data
    }
}
protocol PrintableCommand {
    var value: [UInt8] { get }
}

public enum Command {

    public enum FontControlling: PrintableCommand {

        public enum PrintMode: UInt8 {
            case reset = 0
            case selectFontB = 1 // default font A
            case emphasis = 8
            case doubleHeight = 16
            case dowbleWidth = 32
            case italic = 64
            case underline = 128
        }

        public enum UnderlineMode: UInt8 {
            case enable = 0
            case enable1dot = 1
            case enable2dot = 2
        }

        public enum CharacterFont: UInt8 {
            case a = 0
            case b = 1
            case c = 2
            case d = 3
        }

        public enum CharacterCodePage: UInt8 {
            case ascii = 0
            case cp437 = 3
            case cp808 = 17
            case georgian = 18
        }

        case clear
        case initialize
        case selectPrintMode(PrintMode)
        case underlineMode(UnderlineMode)
        case italicsMode(enable: Bool)
        case emphasis(enable: Bool)
        case selectsCharacterFont(CharacterFont)
        case rotation90(enable: Bool)
        case selectCharacterCodePage(CharacterCodePage)
        case upsiddownMode(enable: Bool)
        // https://escpos.readthedocs.io/en/latest/font_cmds.html#set-cpi-mode-1b-c1-rel
        case setCPIMode(UInt8)
        case reverseMode(enable: Bool)
        // https://escpos.readthedocs.io/en/latest/font_cmds.html#select-double-strike-mode-1b-47-phx
        case doubleStrikeMode(UInt8)

        var value: [UInt8] {
            switch self {
                case .clear, .initialize:
                    return [27, 64]
                case let .selectPrintMode(m):
                    return [27, 33, m.rawValue]
                case let .underlineMode(m):
                    return [27, 45, m.rawValue]
                case let .italicsMode(enable):
                    return [27, 52, enable ? 1 : 0]
                case let .emphasis(enable):
                    return [27, 69, enable ? 1 : 0]
                case let .selectsCharacterFont(font):
                    switch font {
                        case .a, .b:
                            return [27, 77, font.rawValue]
                        case .c:
                            return [27, 84]
                        case .d:
                            return [27, 85]
                    }
                case let .rotation90(enable):
                    return [27, 86, enable ? 1 : 0]
                case let .selectCharacterCodePage(c):
                    return [27, 166, c.rawValue]
                case let .upsiddownMode(enable):
                    return [27, 123, enable ? 1 : 0]
                case let .setCPIMode(n):
                    return [27, 193, n]
                case let .reverseMode(enable):
                    return [29, 66, enable ? 1 : 0]
                case let .doubleStrikeMode(n):
                    return [27, 71, n]
            }
        }
    }

    public enum PrinterInfomation: PrintableCommand {

        public enum PrinterID: UInt8 {
            case modelID = 1
            case typeID = 2
            case firmwareRevision = 3
        }

        case printer(_ id: PrinterID)
        case transmitStatus
        case transmitPaperSensorStatus

        var value: [UInt8] {
            switch self {
                case let .printer(id):
                    return [29, 73, id.rawValue]
                case .transmitStatus:
                    debugPrint("⚠️ TODO://")
                    return [29, 114, 0]
                case .transmitPaperSensorStatus:
                    return [27, 118]
            }
        }
    }

    public enum CursorPosition: PrintableCommand {

        case horizontalTab
        case lineFeed
        case formFeed
        case carriageReturn
        case cancelCurrentLine
        // https://escpos.readthedocs.io/en/latest/cursor_position.html#absolute-print-position-1b-24-rel
        case absolutePrintPosition(nL: UInt8, nH: UInt8)
        // https://escpos.readthedocs.io/en/latest/cursor_position.html#relative-print-position-1b-5c-rel
        case relativePrintPosition(nL: UInt8, nH: UInt8)

        var value: [UInt8] {
            switch self {
                case .horizontalTab:
                    return [9]
                case .lineFeed:
                    return [10]
                case .formFeed:
                    return [12]
                case .carriageReturn:
                    return [13]
                case .cancelCurrentLine:
                    return [24]
                case let .absolutePrintPosition(nL, nH):
                    return [27, 36, nL, nH]
                case let .relativePrintPosition(nL, nH):
                    return [27, 92, nL, nH]
            }
        }
    }

    public enum PageMovement: PrintableCommand {

        case partialCut
        case fullCut
        case ejector
        case print
        case printAndFeed(lines: UInt8)

        var value: [UInt8] {
            switch self {
                case .partialCut:
                    return [27, 105]
                case .fullCut:
                    return [27, 109]
                case .ejector:
                    debugPrint("⚠️ TODO://")
                    return []
                case .print:
                    // 0 will be ignored
                    return [27, 74, 0]
                case let .printAndFeed(lines):
                    return [27, 100, lines]

            }
        }
    }

    public enum Layout: PrintableCommand {

        public enum LineSpace {
            case l1_6
            case l1_8
            case raw(UInt8)
        }

        public enum Alignment: UInt8 {
            case left = 0
            case center = 1
            case right = 2
        }

        case rightSpacing(UInt8)
        case lineSpace(LineSpace)
        case justification(Alignment)

        /**
         -- TODO command
         Left Margin
         Motion Units
         Print Area Width
         */
        var value: [UInt8] {
            switch self {
                case let .rightSpacing(n):
                    return [27, 32, n]
                case let .lineSpace(l):
                    switch l {
                        case .l1_6:
                            return [27, 50]
                        case .l1_8:
                            return [27, 48]
                        case let .raw(n):
                            return [27, 51, n]
                    }
                case let .justification(a):
                    return [27, 97, a.rawValue]
            }
        }
    }

    case style(FontControlling)
    case info(PrinterInfomation)
    case cursor(CursorPosition)
    case page(PageMovement)
    case layout(Layout)

    var value: [UInt8] {
        switch self {
            case let .style(fc):
                return fc.value
            case let .info(i):
                return i.value
            case let .cursor(c):
                return c.value
            case let .page(p):
                return p.value
            case let .layout(l):
                return l.value
        }
    }

}
